﻿using System;

public class LargeObject : IDisposable
{
    private int[] data;

    public LargeObject()
    {
        data = new int[1000000];
    }

    public void UseObject()
    {
        Console.WriteLine("Large object is being used.");
    }

    public void Dispose()
    {
        data = null;

        Console.WriteLine("Large object is disposed.");
    }
}

class Program
{
    static void Main(string[] args)
    {
        using (LargeObject largeObject = new LargeObject())
        {
            largeObject.UseObject();
        }

        Console.WriteLine("Program finished.");
    }
}
