﻿using System;

public class ResourceMonitor
{
    private readonly long maxMemoryUsage; 

    public ResourceMonitor(long maxMemoryUsage)
    {
        this.maxMemoryUsage = maxMemoryUsage;
    }

    public void StartMonitoring()
    {
        Console.WriteLine("Resource monitoring started.");

        while (true)
        {
            long memoryUsage = GC.GetTotalMemory(false);

            if (memoryUsage >= maxMemoryUsage)
            {
                Console.WriteLine("Warning: Memory usage exceeds the maximum allowed level.");
                Console.WriteLine($"Current memory usage: {memoryUsage} bytes");
            }

            System.Threading.Thread.Sleep(1000);
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        long maxMemory = 100000000;

        ResourceMonitor monitor = new ResourceMonitor(maxMemory);
        monitor.StartMonitoring();
    }
}
